import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './partials/footer/footer.component';
import { NavbarComponent } from './partials/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './section/about/about.component';
import { ClientsComponent } from './section/clients/clients.component';
import { ContactComponent } from './section/contact/contact.component';
import { InformationComponent } from './section/information/information.component';
import { TopbannerComponent } from './section/topbanner/topbanner.component';

@NgModule({
	declarations: [
		AppComponent,
		FooterComponent,
		NavbarComponent,
		HomeComponent,
		AboutComponent,
		ClientsComponent,
		ContactComponent,
		InformationComponent,
		TopbannerComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
