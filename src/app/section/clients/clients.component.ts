/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import clients from '../../../assets/data/clients.json';

@Component({
	selector: 'app-clients',
	templateUrl: './clients.component.html',
	styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

	clientsList = [];

	constructor() {}

	ngOnInit(): void {
		let index = 0;
		let auxlist = {
			list: [],
			first: true
		};
		for (const client of clients.list) {
			index += 1;
			auxlist.list = auxlist.list.concat(client);
			if (!(index < 3)) {
				index = 0;
				this.clientsList = this.clientsList.concat(auxlist);
				auxlist = {
					list: [],
					first: false
				};
			}
		}
	}

}
