/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import clients from '../../../assets/data/clients.json';

@Component({
	selector: 'app-information',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.css']
})

export class InformationComponent implements OnInit {
	clientsNum = clients.list.length;
	yearsLive;
	constructor() { }

	ngOnInit(): void {
		const today = new Date();
		this.yearsLive = today.getFullYear() - 1971;
		if (today.getMonth() < 9){
			this.yearsLive = this.yearsLive - 1;
		} else if (today.getMonth() == 9 && today.getDate() < 29){
			this.yearsLive = this.yearsLive - 1;
		}
	}

}
